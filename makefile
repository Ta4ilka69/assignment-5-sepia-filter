CFLAGS := -Wall -Werror -std=c17 -g -O3
SOURCE_DIR := solution/src
OBJ_DIR := build
CC := clang
LD := clang
ASM := nasm
ASMFLAGS := -felf64 -g

OBJECTS := image.o sepia.o bmp.o transform.o util.o mainSepia.o main.o

{OBJ_DIR}:
	mkdir -p $(OBJ_DIR)

main: $(addprefix ${OBJ_DIR}/, $(OBJECTS))
	$(LD) -o $@ $^

${OBJ_DIR}/sepia.o: ${SOURCE_DIR}/sepia.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

${OBJ_DIR}/%.o: ${SOURCE_DIR}/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -rfd $(OBJ_DIR)
	rm -f main
	rm -f *output*.bmp

test: main
	./main input1.bmp output1.bmp input2.bmp output2.bmp input3.bmp output3.bmp input4.bmp output4.bmp input5.bmp output5.bmp
.PHONY: clean test