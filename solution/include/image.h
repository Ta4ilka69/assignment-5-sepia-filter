/*
* header for abstract image
*/
#ifndef IMAGE
#define IMAGE
#include <stdint.h>
struct pixel{
	uint8_t b, g, r;
};
struct image {
	uint64_t width, height;
	struct pixel* data;
};
struct image createImage(uint64_t width, uint64_t height);
void destroyImage(struct image* img);
typedef void(*sepiaFunction)(struct image* sourceImage, struct image* transformedImage);
#endif
