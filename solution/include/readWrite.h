#ifndef readingFile
#define readingFile
#include <stdio.h>

enum readStatus {
	READ_OK = 0,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER,
	READ_SKIPPING_BYTES_ERROR,
	READ_MALLOC_ERROR,
	READ_ERROR_DATA
};
enum writeStatus {
	WRITE_OK = 0,
	WRITE_ERROR,
};
static const char* readErrorMessages[] = {
	"OK",
	"Invalid signature, must be 0x4D42",
	"Invalid bits, must be 24",
	"Invalid header, check your file",
	"Something went wrong while trying to skip padding bits or header",
	"Error occurred while trying to allocate memory for pixel data",
	"Error occurred while trying to read pixel data",
	"Unknown error"
};
static const char* writeErrorMessages[] = {
	"OK",
	"Error occurred while trying to write pixel data",
	"Unknown error"
};
#endif
