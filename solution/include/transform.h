#ifndef TRANSFORM

#include "image.h"
void sepiaFilter(struct image* source, struct image* img);
void sepiaWithSIMD(struct image* source, struct image* img);
#endif
