#include "../include/image.h"
#include "../include/transform.h"
#include <stddef.h>

#define min(a, b) (a) < (b) ? (a) : (b)
void apply_sepia_filter(struct pixel *old, struct pixel *new);
static double rr = 0.393;
static double rg = 0.769;
static double rb = 0.189;
static double gr = 0.349;
static double gg = 0.686;
static double gb = 0.168;
static double br = 0.272;
static double bg = 0.534;
static double bb = 0.131;

static struct pixel pixelTransform(struct pixel *oldp)
{
	uint8_t oldG = oldp->g;
	uint8_t oldR = oldp->r;
	uint8_t oldB = oldp->b;
	uint8_t newR = (uint8_t)(min((int)(rr * oldR + rg * oldG + rb * oldB), 255));
	uint8_t newG = (uint8_t)(min((int)(gr * oldR + gg * oldG + gb * oldB), 255));
	uint8_t newB = (uint8_t)(min((int)(br * oldR + bg * oldG + bb * oldB), 255));
	return (struct pixel){.b = newB, .g = newG, .r = newR};
}

void sepiaFilter(struct image *source, struct image *img)
{
	*img = createImage(source->width, source->height);
	if (img->data == NULL)
	{
		return;
	}
	for (size_t i = 0; i < img->width * img->height; i++)
	{
		struct pixel oldp = (source->data)[i];
		(img->data)[i] = pixelTransform(&oldp);
	}
}

void sepiaWithSIMD(struct image *source, struct image *img)
{
	*img = createImage(source->width, source->height);
	if (img->data == NULL)
	{
		return;
	}
	uint64_t size = img->width * img->height;
	for (size_t i = 0; i < size; i++)
	{
		apply_sepia_filter(source->data+i, img->data+i);
	}

}
