section .text
%define MAX_VALUE 0xff
global apply_sepia_filter

apply_sepia_filter:
    ;loading registers with converted values
    lddqu xmm0, [rdi]
    pmovzxbd xmm0,xmm0 ;to int
    cvtdq2ps xmm0, xmm0 ;to float
    lddqu xmm1, [rel coefficients_b]
    lddqu xmm2, [rel coefficients_g]
    lddqu xmm3, [rel coefficients_r]

    ;first coefficient
    movdqu xmm4, xmm0
    mulps xmm4, xmm1
    haddps xmm4, xmm4
    haddps xmm4, xmm4
    cvtps2dq xmm4, xmm4 ;back to integer
    extractps rcx, xmm4, 0
    cmp rcx, MAX_VALUE
    jle first
    mov rcx, MAX_VALUE
    first:
        mov byte[rsi], cl

    ;second coefficient
    movdqu xmm4, xmm0
    mulps xmm4, xmm2
    haddps xmm4, xmm4
    haddps xmm4, xmm4
    cvtps2dq xmm4, xmm4 ;back to integer
    extractps rcx, xmm4, 0
    cmp rcx, MAX_VALUE
    jle second
    mov rcx, MAX_VALUE
    second:
        mov byte[rsi+1], cl
    ;third coefficient
    movdqu xmm4, xmm0
    mulps xmm4, xmm3
    haddps xmm4, xmm4
    haddps xmm4, xmm4
    cvtps2dq xmm4, xmm4 ;back to integer
    extractps rcx, xmm4, 0
    cmp rcx, MAX_VALUE
    jle third
    mov rcx, MAX_VALUE
    third:
        mov byte[rsi+2], cl
end_processing:
    ret

section .data
coefficients_b: dd 0.131, 0.534, 0.272,0.0
coefficients_g: dd 0.168, 0.686, 0.349,0.0
coefficients_r: dd 0.189, 0.769, 0.393,0.0