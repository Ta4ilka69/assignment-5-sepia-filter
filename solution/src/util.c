#include "../include/image.h"
#include "../include/util.h"

enum fileStatus openFile(FILE** file, const char* filename, const char* mode) {
	*file = fopen(filename, mode);
	if(*file == NULL) {
		return FILE_OPEN_ERROR;
	}
	return FILE_OPEN_OK;
}

enum fileStatus closeFile(FILE* file) {
	if (fclose(file) == 0) {
		return FILE_CLOSE_OK;
	}
	else {
		return FILE_CLOSE_ERROR;
	}
}
void onReturnError(FILE* file, const char* err, struct image * s, struct image * t) {
	printError(err);
	closeFile(file);
	destroyImage(s);
	destroyImage(t);
}
