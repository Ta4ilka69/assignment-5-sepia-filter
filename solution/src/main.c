#include "../include/mainSepia.h"
#include "../include/transform.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char **argv)
{
    size_t num_pairs = (argc - 1) / 2;
    char **asm_args = (char **)malloc(num_pairs * sizeof(char *));
    if (asm_args == NULL) {
        printf("Memory allocation failed.\n");
        return 1;
    }
     for (size_t i = 1; i < argc - 1; i += 2) {
        asm_args[i / 2] = (char *)malloc((strlen(argv[i + 1]) + 5) * sizeof(char));
        if (asm_args[i / 2] == NULL) {
            printf("Memory allocation failed.\n");
            return 1;
        }
        snprintf(asm_args[i / 2], strlen(argv[i + 1]) + 5, "asm_%s", argv[i + 1]);
    }
    clock_t start = clock();
    for(size_t i = 1; i<(argc-1);i+=2){
        int result = mainSepia(argv[i], argv[i+1], sepiaFilter);
        if(result != 0){
            return result;
        }
    }
    clock_t end = clock();
    printf("Time without simd: %f\n", (double)(end - start) / CLOCKS_PER_SEC);
    start = clock();
    for(size_t i = 1; i<(argc-1);i+=2){
        int result = mainSepia(argv[i],asm_args[(int)(i/2)], sepiaWithSIMD);
        if(result != 0){
            return result;
        }
    }
    end = clock();
    printf("Time with simd: %f\n", (double)(end - start) / CLOCKS_PER_SEC);
}