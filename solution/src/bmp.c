#include "../include/bmp.h"


#define BMP_SIGNATURE 0x4D42
#define BMP_BIT_SUPPORT 24
#define BMP_HEADER_SIZE 40

#define calculate_padding(w, p) ((4-(((w)*(p))%4))%4)


enum readStatus fromBmp(FILE* in, struct image* img) {
	struct bmpHeader header;
	
	if(sizeof(struct bmpHeader) != fread(&header, 1, sizeof(struct bmpHeader), in)) {
		return READ_INVALID_HEADER;
	}
	if(header.bfType!= BMP_SIGNATURE) {
		return READ_INVALID_SIGNATURE;
	}
	if(header.biBitCount!=BMP_BIT_SUPPORT) {
		return READ_INVALID_BITS;
	}
	if(header.biWidth<=0 || header.biHeight<=0) {
		return READ_INVALID_HEADER;
	}
	*img = createImage(header.biWidth, header.biHeight);
	if (!img->data) {
		return READ_MALLOC_ERROR;
	}
	if (fseek(in, (long)header.bOffBits, SEEK_SET)) {
		return READ_SKIPPING_BYTES_ERROR;
	}
	uint8_t padding = calculate_padding(img->width, sizeof(struct pixel));
	for (uint64_t i = 0; i < img->height; i++) {
		if(fread(img->data+i*img->width, sizeof(struct pixel), img->width, in)!=img->width) {
			return READ_ERROR_DATA;
		}
		if(fseek(in, padding, SEEK_CUR)) {
			return READ_SKIPPING_BYTES_ERROR;
		}
	}
	return READ_OK;
}

enum writeStatus toBmp(FILE* out, struct image* img) {
	uint8_t padding = calculate_padding(img->width, sizeof(struct pixel));
	struct bmpHeader header = {
		.bfType = BMP_SIGNATURE,
		.bfileSize = (uint32_t)(sizeof(struct bmpHeader) + (sizeof(struct pixel) * img->width + padding) * img->height),
		.bfReserved = 0,
		.bOffBits = sizeof(struct bmpHeader),
		.biSize = BMP_HEADER_SIZE,
		.biWidth = (uint32_t)img->width,
		.biHeight = (uint32_t)img->height,
		.biPlanes = 1,
		.biBitCount = BMP_BIT_SUPPORT,
		.biCompression = 0,
		.biSizeImage = 0,
		.biXPelsPerMeter = 0,
		.biYPelsPerMeter = 0,
		.biClrUsed = 0,
		.biClrImportant = 0
	};
	if (fwrite(&header, sizeof(struct bmpHeader), 1, out) != 1) {
		return WRITE_ERROR;
	}
	const unsigned char paddingBytes[3] = { 0 };
	for (uint64_t i = 0; i < img->height; i++) {
		if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width) {
			return WRITE_ERROR;
		}
		if (fwrite(paddingBytes, 1,padding,out)!=padding) {
			return WRITE_ERROR;
		}
	}
	return WRITE_OK;
}

