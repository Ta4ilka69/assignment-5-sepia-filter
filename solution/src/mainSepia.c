
#include "../include/mainSepia.h"
#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/readWrite.h"
#include <stdlib.h>
#include "../include/util.h"

#define ERROR_CODE 1
#define SUCCESS_CODE 0

int mainSepia(char* sourcePath, char* transformedPath, sepiaFunction f)
{
    FILE *sourceFile = NULL;
    enum fileStatus sourceOpen = openFile(&sourceFile, sourcePath, READ_MODE_BINARY);
    if (sourceOpen != FILE_OPEN_OK)
    {
        printError(fileStatusMessages[sourceOpen]);
        return ERROR_CODE;
    }
    struct image sourceImage;
    enum readStatus sourceRead = fromBmp(sourceFile, &sourceImage);
    if (sourceRead != READ_OK)
    {
        onReturnError(sourceFile, readErrorMessages[sourceRead], &sourceImage, NULL);
        return ERROR_CODE;
    }
    enum fileStatus sourceClose = closeFile(sourceFile);
    if (sourceClose != FILE_CLOSE_OK)
    {
        onReturnError(sourceFile, fileStatusMessages[sourceClose], &sourceImage, NULL);
        return ERROR_CODE;
    }
    struct image transformedImage;
    f(&sourceImage, &transformedImage);
    if (transformedImage.data == NULL)
    {
        onReturnError(NULL, "Error: memory allocation failed", &sourceImage, &transformedImage);
        return ERROR_CODE;
    }
    destroyImage(&sourceImage);
    FILE *transformedFile = NULL;
    enum fileStatus transformedOpen = openFile(&transformedFile, transformedPath, WRITE_MODE_BINARY);
    if (transformedOpen != FILE_OPEN_OK)
    {
        onReturnError(NULL, fileStatusMessages[transformedOpen], NULL, &transformedImage);
        return ERROR_CODE;
    }
    enum writeStatus transformedWrite = toBmp(transformedFile, &transformedImage);
    if (transformedWrite != WRITE_OK)
    {
        onReturnError(transformedFile, writeErrorMessages[transformedWrite], NULL, &transformedImage);
        return ERROR_CODE;
    }
    destroyImage(&transformedImage);
    enum fileStatus transformedClose = closeFile(transformedFile);
    if (transformedClose != FILE_CLOSE_OK)
    {
        onReturnError(NULL, fileStatusMessages[transformedClose], NULL, NULL);
        return ERROR_CODE;
    }
    return SUCCESS_CODE;
}
